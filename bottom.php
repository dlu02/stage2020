<div class="bottom">
	<hr>
	<p>&copy; 2020 <a href="https://perso.dlu02.ovh">Damien LU</a> <br> Stage ENSIIE 1ère année réalisé entre le 08 juin 2020 et le 07 août 2020 au CNAM Paris 3e en télétravail.</p>
	<a href="https://gitlab.com/dlu02/stage2020" class="bottom_droite"><img src="/stage1a/logo_gitlab.png" alt="Lien vers Gitlab" height="30"></a>
	<a href="https://www.ensiie.fr/" class="bottom_droite"><img src="/stage1a/logo_ensiie.png" alt="Lien vers ENSIIE" height="30"></a>
	<a href="http://cnam.fr" class="bottom_droite"><img src="/stage1a/logo_cnam.png" alt="Lien vers ENSIIE" height="20"></a>
	<div style="clear: both;"></div>
</div>
